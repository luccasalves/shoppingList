<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Node.js_logo_2015.svg/2000px-Node.js_logo_2015.svg.png">

# Criação e objetivo
Ryan Dahl criou o [NodeJS](http://www.nodejs.org) em 2009 após perceber que, ao fazer upload de arquivos no Flickr, o navegador não tinha a informação do quanto o arquivo havia sido carregado e precisava consultar o servidor web, então inspirou-se a criar algo que fosse mais fácil e completo.
O NodeJS é um framework desenvolvido na linguagem C++ tendo como base do seu funcionamento a engine do Javascript, chamada [Chrome V8](https://developers.google.com/v8/), desenvolvida pela Google.
Sendo assim, ele contém toda a sintaxe e funcionalidades do Javascript, que funciona no client side (cliente), com a adição de novas funções que permitem o NodeJS ser uma linguagem server side (servidor).
O NodeJS atualmente é um projeto open source e utiliza o gerenciador de pacotes [npm](http://www.npmjs.com) para seu funcionamento.
O objetivo principal da criação deste framework é fornecer uma maneira fácil para construir programas de rede escaláveis.

# Requisitos básicos para utilização
Por ser um framework multiplataforma, a sua utilização é extremamente simples. Todos os pacotes básicos necessários para o início da sua utilização são instalados juntamente ao NodeJS. Abaixo os requisitos básicos para sua utilização:
* Possuir uma plataforma Windows, Linux ou OSX;
* Ter o NodeJS instalado;
* Possuir conhecimento da sintaxe do gerenciador de pacotes npm para controle das dependências.

# Documentação
Suas principais documentações são escritas somente em inglês e estão disponíveis nos sites a seguir:

* [Site oficial com a documentação da versão atual](https://nodejs.org/docs/v8.1.0/api) 
* [Repositório onde se encontra o código oficial do NodeJS juntamente com suas principais informações](https://github.com/nodejs/node)
* [Documentação sobre a engine utilizada no NodeJS](https://developers.google.com/v8) 
* [Repositório onde se encontra o código oficial do Chrome V8 Engine juntamente com suas principais informações](https://github.com/v8/v8)   
* [Site com documentações complementares](http://devdocs.io/node/)
* [Site oficial do Express, framework muito utilizado em aplicações NodeJS](http://expressjs.com/pt-br/)

# Como instalar
Segue abaixo o passo a passo para instalação do NodeJS (utilizando o framework Express):
1. Realizar o download do NodeJS através do [site oficial](https://nodejs.org/en/download);
1. Instalar o NodeJS seguindo as orientações do instalador;
1. Criar o diretório para sua aplicação através do Prompt de Comando;
1. Exetutar o comando `npm install -g express-generator` para instalar o framework Express;
1. Executar o comando `express --ejs` ([EJS](http://www.embeddedjs.com/) = template do Javascript);
1. Executar o comando `npm install` para instalar as dependências da aplicação;
1. Executar o comando `npm start` e acessar sua aplicação em [http://localhost:3000](http://localhost:3000).

# Vantagens
Algumas das principais vantagens de utilizar NodeJS são:
* Framework Open Source com comunidade ativa;
* Permite grande escalabilidade em aplicações I/O intensive;
* Uso de uma única linguagem de programação (Javascript) para desenvolvimento de uma aplicação completa(client side e server side);
* Menor curva de aprendizado para os desenvolvedores que já possuem conhecimento prévio em Javascript;
* Frameworks que interagem em tempo real entre cliente e servidor permitem tráfegos de dados através de uma única conexão bi-direcional (thread), tratando as mensagens via eventos no Javascript;
* Utilização por grandes corporações, como Linkedin, Microsoft e PayPal. 

# Desvantagens
Não foi encontrado nenhuma desvantagem na utilização do framework durante a execução do trabalho.

## Equipe
1. [Alana Ramires Corrêa](@alanarcorrea)
1. [Lucas Alves](@luccasalves)

## Vídeo
Link: https://youtu.be/H_kmEHblVhA

