// Chamada das dependências necessárias
var express = require('express');
var router = express.Router();
var mysql = require('mysql');

// Criação da conexão com o banco de dados
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'shoppingList'
});

// Conexão ao banco de dados
connection.connect();

// Rota get para renderização do index passando parâmetros para a view
// Parâmetro req = request
// Parâmetro res = response/retorno
// Parâmetro next = função de callback
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// Rota get para listem de itens vindos do banco de dados
router.get('/list', function(req, res, next) {
  // Linha de comando que será executada no banco de dados 
  var sql = 'SELECT * FROM items';

  // Execuxão da query no banco de dados
  // Parâmetro err = parâmetro de erro
  // Parâmetro rows = linhas dos dados vindos do banco
  // Parâmetro fields = campos de dados vindos do banco
  connection.query(sql, function(err, rows, fields) {
    if (!err) { //teste de erro da query
      res.json({'list': rows}); //retorno das linhas da query realizada no banco de dados em formato json
    } else {
      console.log('ERRO! #####'); //mensagem de erro no console
    }
  });
});

// Rota post para inserção dos dados no banco
router.post('/list', function(req, res, next) {

  var items = req.body.items; // Variável items recebe itens vindos da request feita no frontend
  var name = '';
  var quant = '';
  var sql = '';
  var list = [];

  // Execução da query para limpar os dados da tabela items no banco de dados
  connection.query('TRUNCATE table items');

  // Inserindo itens recebidos do frontend no banco de dados
  for (var i = 0; i < items.length; i++) {
    name = req.body.items[i].name;
    quant = req.body.items[i].quant;
    sql = 'INSERT INTO items(name, quant) VALUES ("' + name + '", ' + quant + ');';

    // Execução da query para de inserção de items
    connection.query(sql, function(err, rows, fields) {
      if (!err) {
        console.log('OK:', i); // Mensagem de sucesso no console
      } else {
        console.log('ERROR:', i); // Mensagem de erro no console
      }
    });
  }

  res.json({'response': true}); // Resposta ao frontend com mensagem de sucesso em formato json
});

module.exports = router;