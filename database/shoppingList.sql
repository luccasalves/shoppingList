DROP DATABASE IF EXISTS shoppingList;

CREATE DATABASE shoppingList;

USE shoppingList;

CREATE TABLE IF NOT EXISTS items(
    id INT(8) AUTO_INCREMENT,
    name VARCHAR(250) NOT NULL,
    quant INT(8) NOT NULL,

    PRIMARY KEY (id)
);