ST = {
	init: function() {
		this.binds();
		this.getData();
	},

	binds: function() {
		$(document).on('focusin', '.shoppingList__item .text', this.handlerFocus.bind(this));
		$(document).on('focusout', '.shoppingList__item .text', this.handlerFocus.bind(this));
		$(document).on('keyup', '.text', this.handlerText.bind(this));
		$('.add').on('click', this.add.bind(this));
		$('.remove').on('click', this.remove.bind(this));
		$('.button').on('click', this.send.bind(this));
	},

	getData: function() {
		$('.shoppingList').empty();

		// Chamada get para a rota /list
		$.ajax({
			url: '/list',
			type: 'GET',
			dataType: 'json',
			processData: false,
			contentType: 'application/json; charset=utf-8',
		})
		.done(function(data) {
			var container = $('.shoppingList');
			var template = '<div class="shoppingList__item active" data-id="2"><div class="col left"><label class="lbl">Nome:</label><input type="text" class="text name" value="{{NAME}}"><span class="line"></span></div><div class="col right"><label class="lbl">Quantidade:</label><input type="text" class="text quant" value="{{QUANT}}"><span class="line"></span></div></div>';

			if (data.list.length == 0) {
				$('.empty').show();
				container.hide();
			} else {
				$('.empty').hide();
				container.show();
			}

			for (var i = 0; i < data.list.length; i++) {
				var item = template
							.replace('{{NAME}}', data.list[i].name)
							.replace('{{QUANT}}', data.list[i].quant);

				container.append(item);
			}
		})
		.fail(function(data) {
			console.log('ERROR:', data);
		})
		.always(function() {
			
		});
	},

	handlerFocus: function(event) {
		var target = $(event.currentTarget);

		var label = target.closest('div').find('.lbl');
		var line = target.closest('div').find('.line');
		var text = target.closest('div').find('.text');

		if (text.val() == '') {
			switch(event.type) {
				case 'focusin':
					TweenMax.to(label, .3, {css: {top: '0'}, ease: Quint.ease});
					TweenMax.to(line, .3, {css: {width: '100%'}, ease: Quint.ease});
					break;
				case 'focusout':
					TweenMax.to(label, .3, {css: {top: '30px'}, ease: Quint.ease});
					TweenMax.to(line, .3, {css: {width: '0%'}, ease: Quint.ease});
					break;
			}
		}
	},

	handlerText: function(event) {
		var _this = this;
		var target = $(event.currentTarget);

		if (target.val() != '') {
			target.closest('div').addClass('active');
		} else {
			target.closest('div').removeClass('active');
		}
	},

	add: function() {
		var container = $('.shoppingList');
		var template = '<div class="shoppingList__item" data-id="2"><div class="col left"><label class="lbl">Nome:</label><input type="text" class="text name"><span class="line"></span></div><div class="col right"><label class="lbl">Quantidade:</label><input type="text" class="text quant"><span class="line"></span></div></div>';

		$('.empty').hide();
		container.show();

		container.append(template);
	},

	remove: function() {
		var last = $('.shoppingList__item').last();

		if ($('.shoppingList__item').length -1 == 0) {
			$('.empty').show();
		}

		last.remove();
	},

	send: function() {
		var _this = this;
		var list = [];
		var item = $('.shoppingList__item');

		for (var i = 0; i < item.length; i++) {
			var listItem = {
				name: $(item[i]).find('.name').val(),
				quant: $(item[i]).find('.quant').val(),
			}

			list.push(listItem);
		}

		var itemsList = {
			items: list
		}

		// Chamada post para /list
		$.ajax({
			url: '/list',
			type: 'post',
			dataType: 'json',
			processData: false,
			contentType: 'application/json; charset=utf-8',
			data: JSON.stringify(itemsList),
		})
		.done(function(data) {
			if (data.response) {
				_this.showStatus();
			}
		})
		.fail(function(data) {
			console.log('ERROR:', data);
		})
		.always(function() {
			
		});
	},

	showStatus: function() {
		var _this = this;
		$('.status').show();

		setTimeout(function() {
			_this.closeStatus();
		}, 4000);
	},

	closeStatus: function() {
		$('.status').fadeOut('slow');
	}
}

$(document).ready(function() {
	ST.init();
});